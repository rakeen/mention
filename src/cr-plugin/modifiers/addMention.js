import { Modifier, EditorState } from 'draft-js';
import getSearchText from '../utils/getSearchText';
import getTypeByTrigger from '../utils/getTypeByTrigger';
import { addEntity } from '../utils/updateDisplay';

const getEntities = (editorState, entityType = null) => {
  const content = editorState.getCurrentContent();
  const entities = [];
  content.getBlocksAsArray().forEach((block) => {
    let selectedEntity = null;
    block.findEntityRanges(
      (character) => {
        if (character.getEntity() !== null) {
          const entity = content.getEntity(character.getEntity());
          if (!entityType || (entityType && entity.getType() === entityType)) {
            selectedEntity = {
              entityKey: character.getEntity(),
              blockKey: block.getKey(),
              entity: content.getEntity(character.getEntity()),
            };
            return true;
          }
        }
        return false;
      },
      (start, end) => {
        entities.push({ ...selectedEntity, start, end });
      });
  });
  return entities;
};


/**
 * similar to `addMention()`
 * 
 * given `editorState`, find the `entityList`
 * decide which substring to substitute with the mention.Text  
 * 
 */
export const tokenizeDisplay = (editorState, mention, mentionPrefix, mentionTrigger, entityMutability, suggestions) => {
  if(suggestions.length===1){
    let lastEntity = getEntities(editorState).pop();
    const plainText = editorState.getCurrentContent().getPlainText();

    let _begin = lastEntity ? lastEntity.end + 1 : 0;

    console.log("INSIDE",suggestions);
    let replacableRange = plainText.substr(_begin).trim();
    if(replacableRange.endsWith(suggestions[0].name)) _begin = plainText.length - suggestions[0].name.length;
  

    const contentStateWithEntity = editorState.getCurrentContent().createEntity(
      getTypeByTrigger(mentionTrigger), entityMutability, { mention }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

    const currentSelectionState = editorState.getSelection();
    let { begin, end } = getSearchText(editorState, currentSelectionState, mentionTrigger);
    begin = _begin;
    console.log("begin: ", lastEntity, begin)
    // get selection of the @mention search text
    const mentionTextSelection = currentSelectionState.merge({
      anchorOffset: begin,
      focusOffset: end,
    });

    let mentionReplacedContent = Modifier.replaceText(
      editorState.getCurrentContent(),
      mentionTextSelection,
      `${mentionPrefix}${mention.name}`,
      null, // no inline style needed
      entityKey
    );

    // If the mention is inserted at the end, a space is appended right after for
    // a smooth writing experience.
    // const blockKey = mentionTextSelection.getAnchorKey();
    // const blockSize = editorState.getCurrentContent().getBlockForKey(blockKey).getLength();
    // if (blockSize === end) {
    //   mentionReplacedContent = Modifier.insertText(
    //     mentionReplacedContent,
    //     mentionReplacedContent.getSelectionAfter(),
    //     ' ',
    //   );
    // }

    const newEditorState = EditorState.push(
      editorState,
      mentionReplacedContent,
      'insert-mention',
    );
    return EditorState.forceSelection(newEditorState, mentionReplacedContent.getSelectionAfter());
  }
  else return editorState;
}



const addMention = (editorState, mention, mentionPrefix, mentionTrigger, entityMutability) => {

  console.log(getEntities(editorState));

  let lastEntity = getEntities(editorState).pop();
  let _begin = lastEntity ? lastEntity.end + 1 : 0;

  const contentStateWithEntity = editorState.getCurrentContent().createEntity(
    getTypeByTrigger(mentionTrigger), entityMutability, { mention }
  );
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();

  const currentSelectionState = editorState.getSelection();
  let { begin, end } = getSearchText(editorState, currentSelectionState, mentionTrigger);
  begin = _begin;
  console.log("begin: ", lastEntity, begin)
  // get selection of the @mention search text
  const mentionTextSelection = currentSelectionState.merge({
    anchorOffset: begin,
    focusOffset: end,
  });

  let mentionReplacedContent = Modifier.replaceText(
    editorState.getCurrentContent(),
    mentionTextSelection,
    `${mentionPrefix}${mention.name}`,
    null, // no inline style needed
    entityKey
  );

  // If the mention is inserted at the end, a space is appended right after for
  // a smooth writing experience.
  const blockKey = mentionTextSelection.getAnchorKey();
  const blockSize = editorState.getCurrentContent().getBlockForKey(blockKey).getLength();
  if (blockSize === end) {
    mentionReplacedContent = Modifier.insertText(
      mentionReplacedContent,
      mentionReplacedContent.getSelectionAfter(),
      ' ',
    );
  }

  const newEditorState = EditorState.push(
    editorState,
    mentionReplacedContent,
    'insert-mention',
  );
  return EditorState.forceSelection(newEditorState, mentionReplacedContent.getSelectionAfter());
};


export const addTokenOnSuggestionSelect = (editorState, entityArray, mention, caretPos) => {
  // find which range to repalce
  let anchorOffset = 0;
  let focusOffset = 0;
  let idx = 0;
  for(let i=0;i<entityArray.length;i++){
    focusOffset = entityArray[i].end;
    if(entityArray[i].start <= caretPos && caretPos <= entityArray[i].end ){
      anchorOffset = entityArray[i].start;
      idx=i;
      break;
    }
  }
  idx = idx -1;
  // @todo need to review the logic
  // @todo fix the backend first
  // *maybe not* if this mention text starts with the very first prefix INCOMPOETE token then replace with that
  while(idx>=0 && (entityArray[idx].isExtensible || entityArray[idx].type==='INCOMPLETE')){
    anchorOffset = entityArray[idx].start;
    idx--;
  }

  const contentState = editorState.getCurrentContent();
  const currentSelectionState = editorState.getSelection();
  // which range to replace
  const mentionTextSelection = currentSelectionState.merge({
      anchorOffset,
      focusOffset
  });
  // create displayToken from suggestion object
  const entity = {
    text: mention.name,
    start: focusOffset,
    end: focusOffset+mention.name.length,
    type: mention.token_type,
    isExtensible: false
  }
  const contentStateWithEntity = contentState.createEntity(
      entity.type, 'MUTABLE', entity
  );
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  let mentionReplacedContent = Modifier.replaceText(
      contentState,
      mentionTextSelection,
      entity.text,
      null, // no inline style needed
      entityKey
  );

  console.log(entityArray)
  console.log("mention select ", anchorOffset, focusOffset, caretPos, mention)
  editorState = EditorState.push(
    editorState,
    mentionReplacedContent,
    'insert-mention',
  );
  EditorState.forceSelection(editorState, mentionReplacedContent.getSelectionAfter());
  return editorState;
}

/**
 * @todo how do you preserve the caret position?
 */
export const removeTokenOnClick = (editorState, contentState, mention, prevSelectionState) => {
  // editorState = EditorState.forceSelection(editorState, prevSelectionState);
  let currentSelectionState = editorState.getSelection();
  console.log("gg ", mention)
  console.log("gg ", currentSelectionState.toJS())
  console.log("gg ", editorState.getCurrentContent().getPlainText())
  const mentionTextSelection = currentSelectionState.merge({
      anchorOffset: mention.start,
      focusOffset: mention.end
  });
  console.log("gg ", mentionTextSelection.toJS())
  
  // let mentionReplacedContent = Modifier.replaceText(
  //   contentState,
  //   mentionTextSelection,
  //   '',
  //   null, // no inline style needed
  //   null
  // );

  let mentionReplacedContent = Modifier.removeRange(
    contentState,
    mentionTextSelection,
    'forward'
  );

  // return mentionReplacedContent;
  // let mentionReplacedContent;
  // console.log(entityArray)
  // console.log("mention select ", anchorOffset, focusOffset, caretPos, mention)
  editorState = EditorState.push(
    editorState,
    // might want to create a new func, because these two case might be different 
    mentionReplacedContent,
    'delete-token',
  );
  // const content = editorState.getCurrentContent();
  //   const entities = [];
  //   content.getBlocksAsArray().forEach((block) => {
  //       let selectedEntity = null;
  //       block.findEntityRanges(
  //       (character) => {
  //           if (character.getEntity() !== null) {
  //               const entity = content.getEntity(character.getEntity());
  //               selectedEntity = {
  //                   entityKey: character.getEntity(),
  //                   blockKey: block.getKey(),
  //                   entity: content.getEntity(character.getEntity()),
  //               };
  //               return true;
  //           }
  //           return false;
  //       },
  //       (start, end) => {
  //           entities.push({ ...selectedEntity, start, end });
  //       });
  //   });
  //   console.log("%c 12 %s", "background: #3ca", currentSelectionState.anchorOffset)
  //   console.log("%c 12 %s", "background: #3ca", prevSelectionState.anchorOffset)
  //   currentSelectionState = currentSelectionState.merge({
  //     anchorOffset: prevSelectionState.anchorOffset-mention.text.length+1,
  //     focusOffset: prevSelectionState.focusOffset-mention.text.length+1
  //   })
  //   console.log(":- ", entities, currentSelectionState.anchorOffset)
  //   editorState = EditorState.forceSelection(editorState, currentSelectionState);
  return editorState;
}

export default addMention;
