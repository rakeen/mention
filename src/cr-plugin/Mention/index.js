import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import clsx from 'clsx';
import { removeTokenOnClick } from '../modifiers/addMention';
import { debug } from 'util';
const MentionLink = ({ mention, children, className }) =>
  <a
    href={mention.link}
    className={className}
    spellCheck={false}
  >
    {children}
  </a>;

const MentionText = ({ children, className }) =>
  <span
    className={className}
    spellCheck={false}
  >
    {children}
  </span>;

/**
 * 
 * props doc: https://github.com/facebook/draft-js/blob/master/src/model/decorators/DraftDecorator.js
 */
const Mention2 = (props) => {
  const {
    entityKey,
    theme = {},
    mentionComponent,
    children,
    decoratedText,
    className,
    contentState
  } = props;

  let { type } = contentState.getEntity(entityKey).getData();

  const combinedClassName = clsx(
    //theme.mention, // for CR style, need to refactor and make configurable
    className, 
    type.toLowerCase()
  );
  const mention = contentState.getEntity(entityKey).getData().mention;

  const Component = (
    mentionComponent || (mention.link ? MentionLink : MentionText)
  );
  return (
    <Component
      entityKey={entityKey}
      mention={mention}
      theme={theme}
      className={combinedClassName}
      decoratedText={decoratedText}
    >
      {children}
    </Component>
  );
};

// Draft.js reuses these components
class DisplayToken extends Component {
  constructor(props){
    super(props);
    console.log("$^ ", this.props.mention)
    this.state = {
      removed: false
    }
  }
  state = {
    removed: false
  };
  componentWillMount(){
    console.log("mounting: ", this.state.removed)
  }
  componentWillUnmount = () => {
    console.log("gg")
    this.thisComponent.removeEventListener('transitionend', this._onTransitionEnd);
  }
  componentDidUpdate(){
    console.log("update", this.state.removed)
  }
  componentWillReceiveProps(nextProps){
    if(this.props.children[0].props.text !== nextProps.children[0].props.text){
      console.log("zxc")
      // this.setState({ removed: false});
    }
  }
  _handleClick = (e) => {

    // for preserving caret position
    let prevSelectionState = this.props.getEditorState().getCurrentContent().getSelectionBefore();
    console.log("%c 12 %s", "background: #3c0", this.props.getEditorState().getCurrentContent().getSelectionAfter().anchorOffset);
    console.log("%c 12 %s", "background: #3c0", this.props.getEditorState().getSelection().anchorOffset);
    console.log("%c 12 %s", "background: #3c0", this.props.getEditorState().getSelection().focusOffset);
    // console.log("12 ", this.props.getEditorState().getUndoStack().toJS().map(d=> {return {...d, "xx": d.getSelection()}}));
    console.log("12 ", this.props.getEditorState().getUndoStack());
    // console.log(this.props.getEditorState().getCurrentContent().getSelectionAfter().anchorOffset)
    console.log(this.props.getEditorState().getSelection().anchorOffset)
    e.persist();
    e.preventDefault();
    e.stopPropagation();
    console.log("clicked")
    console.log(e.target, ReactDOM.findDOMNode(this), this.thisComponent, this.state.removed)
    console.log(e)
    // alternatively you can use `ref`: https://stackoverflow.com/a/36181732/4437655
    // pass value https://stackoverflow.com/a/23024673/4437655
    this.thisComponent.addEventListener('transitionend', this._onTransitionEnd.bind(null, e, prevSelectionState));
    // e.target.addEventListener('transitionend', this._onTransitionEnd);
    this.setState({
      removed: true
    }, ()=>{
      console.log("setState after click")
      console.log(e.target, ReactDOM.findDOMNode(this), this.thisComponent)
    });
    return false;
  }
  _onTransitionEnd = (e, prevSelectionState) => {
    this.setState({
      removed: false
    });
    console.log("LL ",this.props.getEditorState)
    // this.props.removeElement();
    console.log(this.props.mention)
    this.props.setEditorState(
      removeTokenOnClick(this.props.getEditorState(), this.props.contentState, this.props.mention, prevSelectionState)
    );
    console.log("transition ended")
    return false;
  }
  render(){
    let xxx = this.state.removed ? this.props.className+" removed token-close-container" : this.props.className+" token-close-container";
    return (
      <div 
        ref={elem => this.thisComponent = elem}
        style={{
          // display: "inline"
        }}
        className={xxx}
      >
        {this.props.children}
        <span
          onClick = {this._handleClick}
          className = "token-close-btn"
        >×</span>
      </div>
    )
  }
}

/**
 * For a full list of props for `DraftDecoratorComponent`
 * @see https://github.com/facebook/draft-js/blob/master/src/model/decorators/DraftDecorator.js#L53-L73
 */
class Mention extends Component {

  constructor(props) {
    super(props);
    const {
      entityKey,
      theme = {},
      mentionComponent,
      children,
      decoratedText,
      className,
      contentState,
    } = props;
    let { type } = contentState.getEntity(entityKey).getData();
    const combinedClassName = clsx(
      //theme.mention, // for CR style, need to refactor and make configurable
      className, 
      type.toLowerCase()
    );
    // const mention = contentState.getEntity(entityKey).getData().mention;
  
    // const Component = (
    //   mentionComponent || (mention.link ? MentionLink : MentionText)
    // );
  }
  state = {
    removeElement: false
  };
  _removeElement = () => {
    this.setState({ removeElement: true});
  }
  componentWillUnmount(){
    console.log(this.props.contentState.getEntity(this.props.entityKey).getData())
    console.log("unmouning")
  }
  render(){
    if(this.state.removeElement) return null;

    // @see https://github.com/facebook/draft-js/blob/master/src/model/decorators/DraftDecorator.js#L53-L73
    const {
      entityKey,
      theme = {},
      mentionComponent,
      children,
      decoratedText,
      className,
      contentState,
      store,
      start,
      end
    } = this.props;
    let entity = contentState.getEntity(entityKey).getData();
    entity = {
      ...entity,
      start,
      end
    }
    let { type } = entity;
    // console.log("$^ ",contentState.getEntity(entityKey).getData())
    const combinedClassName = clsx(
      //theme.mention, // for CR style, need to refactor and make configurable
      className, 
      type.toLowerCase()
    );
  
    const Component = (
      DisplayToken || mentionComponent || (entity.link ? MentionLink : MentionText)
    );
    return (
      <Component
        entityKey={entityKey}
        mention={entity}
        theme={theme}
        className={combinedClassName}
        decoratedText={decoratedText}
        setEditorState={store.setEditorState}
        getEditorState={store.getEditorState}
        contentState={contentState}
        removeElement={this._removeElement}
      >
        {children}
      </Component>
    )
  }
}

export default Mention;
