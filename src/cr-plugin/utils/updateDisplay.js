import { Modifier, EditorState } from 'draft-js';
import getEntityArray from './getEntityArray';
import {SelectionState} from 'draft-js';
export const addEntity = (editorState, entity) => {
    // console.log("# ", entity.end, " addEntity ", entity)
    let contentState = editorState.getCurrentContent();
    const currentSelectionState = editorState.getSelection();
    const mentionTextSelection = currentSelectionState.merge({
        anchorOffset: entity.start,
        focusOffset: entity.end
    });
    // contentState = Modifier.applyEntity(
    //     contentState,
    //     mentionTextSelection,
    //     null
    // );
    const contentStateWithEntity = contentState.createEntity(
        // entity.type, 'MUTABLE', { ...entity }
        entity.type, 'MUTABLE', { ...entity }
    );
    
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    

    let mentionReplacedContent = Modifier.replaceText(
        contentState,
        mentionTextSelection,
        entity.text,
        null, // no inline style needed
        entityKey
    );
    
    return mentionReplacedContent;
}

export default (editorState, displayTokens) => {
    let prevEntityArray = getEntityArray(editorState);
    let incompleteEntities = [];
    prevEntityArray.forEach( (e,idx)=>{
        if(e.type==='INCOMPLETE'){
            incompleteEntities.push({
                idx: idx,
                ...e
            });
        }
        
        // if this is complete and the displayToken is not complete
        if(e.type!=='INCOMPLETE' && displayTokens && displayTokens[idx].type==='INCOMPLETE'){
            let mentionReplacedContent = addEntity(editorState, displayTokens[idx])
            editorState = EditorState.push(
                editorState,
                addEntity(editorState, displayTokens[idx]),
                'insert-mention',
            );
            //editorState = EditorState.forceSelection(editorState, mentionReplacedContent.getSelectionAfter());
            
        }
    });
    let mentionReplacedContent;
    incompleteEntities.forEach((e)=>{
        if(e.type==='INCOMPLETE' && displayTokens && displayTokens[e.idx].type!=='INCOMPLETE'){
            mentionReplacedContent = addEntity(editorState, displayTokens[e.idx])
            editorState = EditorState.push(
                editorState,
                addEntity(editorState, displayTokens[e.idx]),
                'insert-mention',
            );          
            // editorState = EditorState.forceSelection(editorState, mentionReplacedContent.getSelectionAfter());
        }
        else{
            // let entity = displayTokens[e.idx];
            // let contentState = editorState.getCurrentContent();
            // const currentSelectionState = editorState.getSelection();
            // const mentionTextSelection = currentSelectionState.merge({
            //     anchorOffset: entity.start,
            //     focusOffset: entity.end
            // });
            // console.log("b ", entity)
            // const contentStateWithEntity = contentState.createEntity(
            //     // entity.type, 'MUTABLE', { ...entity }
            //     entity.type==='COMPLETE' ? 'mention' : 'INCOMPLETE', 'MUTABLE', { ...entity }
            // );
            // const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
            // contentState = Modifier.applyEntity(
            //     contentState,
            //     mentionTextSelection,
            //     entityKey
            // );
            mentionReplacedContent = addEntity(editorState, displayTokens[e.idx])
            editorState = EditorState.push(
                editorState,
                mentionReplacedContent,
                'insert-mention',
            );
            editorState = EditorState.forceSelection(editorState, mentionReplacedContent.getSelectionAfter());
        }
    });
    
    return editorState;
};