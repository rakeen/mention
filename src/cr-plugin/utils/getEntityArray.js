export default (editorState) => {
    const content = editorState.getCurrentContent();
    const entities = [];
    content.getBlocksAsArray().forEach((block) => {
        let selectedEntity = null;
        block.findEntityRanges(
        (character) => {
            if (character.getEntity() !== null) {
                const entity = content.getEntity(character.getEntity());
                selectedEntity = {
                    entityKey: character.getEntity(),
                    blockKey: block.getKey(),
                    entity: content.getEntity(character.getEntity()),
                };
                return true;
            }
            return false;
        },
        (start, end) => {
            entities.push({ ...selectedEntity, start, end });
        });
    });

    const plainText = content.getPlainText();
    
    let prev = 0;
    let entityArray = [];
    entities.forEach( (entity,idx)=>{
        let { type, start, end, isExtensible } = entity.entity.getData()
        //console.log("# ", entity.entity.getData(), entity)
        if(prev!==entity.start){
            if(type==='INCOMPLETE' && idx > 0 && entityArray[idx-1].type==='INCOMPLETE'){
                entityArray[idx-1].end = entity.end;
                entityArray[idx-1].text = entityArray[idx-1].text + plainText.substring(prev, entity.end);
            }
            else if(type==='INCOMPLETE'){
                entityArray.push({
                    start: prev,
                    end: entity.end,
                    type: 'INCOMPLETE',
                    text: plainText.substring(prev, entity.end),
                    isExtensible
                });
            }
            else{
                entityArray.push({
                    start: prev,
                    end: entity.start,
                    type: 'INCOMPLETE',
                    text: plainText.substring(prev, entity.start),
                    isExtensible
                });
            }
        }
        else{
            if(type==='INCOMPLETE' && idx > 0 && entityArray[idx-1].type==='INCOMPLETE'){
                entityArray[idx-1].end = entity.end;
                entityArray[idx-1].text = entityArray[idx-1].text + entity.entity.getData().text;
            }
            else entityArray.push({
                start: entity.start,
                end: entity.end,
                type,
                text: plainText.substring(entity.start, entity.end),
                isExtensible
            });
        }
        prev = entity.end;
    });

    if(prev!==plainText.length){
        if(entityArray.length>0 && entityArray[entityArray.length-1].type==='INCOMPLETE'){
            entityArray[entityArray.length-1].end = plainText.length;
            entityArray[entityArray.length-1].text = entityArray[entityArray.length-1].text + plainText.substring(prev, plainText.length);
        }
        else{
            console.log("# ", prev, plainText, entityArray)
            entityArray.push({
                start: prev,
                end: plainText.length,
                type: 'INCOMPLETE',
                text: plainText.substring(prev, plainText.length),
                isExtensible: true // not sure if it is true or false or undefined
            });
        }
    }
    return entityArray;
};
