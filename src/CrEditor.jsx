import React, { Component } from 'react';
import { EditorState } from 'draft-js';
import Editor from 'draft-js-plugins-editor';
import createSingleLinePlugin from 'draft-js-single-line-plugin';
// import createMentionPlugin from 'draft-js-mention-plugin';
//import createMentionPlugin from 'draft-js-cr-plugin';
import createMentionPlugin from './cr-plugin';

import './crStyles.css';
import editorStyles from './editorStyles.module.css';
import mentionsStyles from './mentionsStyles.module.css';
import { debounce } from 'lodash';
import { tokenizeDisplay } from './cr-plugin/modifiers/addMention';

const Entry = (props) => {
    const {
        mention,
        theme,  
        searchValue, // eslint-disable-line no-unused-vars
        isFocused, // eslint-disable-line no-unused-vars
        ...parentProps
    } = props;

    return (
        <div {...parentProps} className={isFocused ? "suggestionItemFocused": ""}>
            <div className={theme.mentionSuggestionsEntryContainer}>
                <div className={theme.mentionSuggestionsEntryContainerLeft}>
                    <img
                    src="https://img.icons8.com/metro/344/a-lowercase.png"
                    //className={theme.mentionSuggestionsEntryAvatar}
                    role="presentation"
                    height="10"
                    style={{ margin: "5px"}}
                    />
                </div>

                <div className={theme.mentionSuggestionsEntryContainerRight}>
                    <div className={theme.mentionSuggestionsEntryText}>
                        {mention.name}
                    </div>
        
                    <div className={theme.mentionSuggestionsEntryTitle} style={{fontSize: "8px"}}>
                        {mention.token_type}
                    </div>
                    <div className={theme.mentionSuggestionsEntryTitle} style={{fontSize: "8px"}}>
                        {mention.table}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default class CrEditor extends Component {

  constructor(props) {
    super(props);

    this.mentionPlugin = createMentionPlugin({
        mentionPrefix: '',
        mentionTrigger: '',
        supportWhitespace: false,
        theme: mentionsStyles,
        mentionComponent: (mentionProps) => {
            return (
                <span
                className={`${mentionProps.className} token-close-container`}
                >
                {mentionProps.children}
                <span className="token-close-btn">×</span>
                </span>
            );
        },
    });
  }

  state = {
    editorState: EditorState.createEmpty(),
    suggestions: [],
    // displayTokens: [
    //     {
    //         "type": "COMPLETE",
    //         "text": "OrderID",
    //         "start": 0,
    //         "end:": 6,
    //         "idx": 0
    //     }
    // ]
  };

  onChange = (editorState) => {
    this.setState({
      editorState,
    });
  };

  onSearchChange = ({ value, entityArray, caretPos }) => {
    // console.log({value, entityArray})
    // if(caretPos===7){
    //         entityArray = [
    //         {
    //                     "type": "COMPLETE",
    //                     "text": "OrderID",
    //                     "start": 0,
    //                     "end": 7,
    //                     "idx": 0
    //                 }
    //     ];
    //     console.log("asd")
    // }
    // this.setState({
    //     suggestions: [
    //         {
    //             "name": "OrderID",
    //             "value": "OrderID",
    //             "title": "OrderID",
    //             "token_type": "TABLE_NAME",
    //             "table": "Orders"
    //         },
    //         {
    //             "name": "CustomerID",
    //             "value": "CustomerID",
    //             "token_type": "TABLE_NAME",
    //             "table": "Orders"
    //         }
    //     ],
    //     displayTokens: [...entityArray]
    // });
    // return;
    // console.log(value)
    const url = `http://localhost:3300`;
    if(this.inFlightReq) this.inFlightReq.abort();
    let xhr = new XMLHttpRequest();   // new HttpRequest instance 
    this.inFlightReq = xhr;
    xhr.open("POST", url);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify({
        "search_text": value,
        entityArray,
        caretPos
    }));
    xhr.onload = ()=>{
        console.log(" a ", xhr.response)
        this.setState(JSON.parse(xhr.response));
    };
    
    
    // fetch(url, {
    //     method: 'POST',
    //     mode: 'cors',
    //     headers: {
    //         'Content-Type': 'application/json'
    //     },
    //     body: JSON.stringify({
    //         "search_text": value,
    //         entityArray,
    //         caretPos
    //     })
    // })
    // .then((response) => response.json())
    // .then(({suggestions, displayTokens}) => {
    //     this.setState({
    //         suggestions,
    //         displayTokens
    //     });
    // });
  };

  focus = () => {
    this.editor.focus();
  };

  render() {
    const handleReturnAfterPlugins = {
        handleReturn: (e) => {
            // fire search here
            return 'handled';
        }
    };
    const { MentionSuggestions } = this.mentionPlugin;
    const singleLinePlugin = createSingleLinePlugin();
    const plugins = [this.mentionPlugin, handleReturnAfterPlugins];

    return (
      <div className={editorStyles.editor} onClick={this.focus}>
        <Editor
          editorState={this.state.editorState}
          onChange={this.onChange}
          plugins={plugins}
          ref={(element) => { this.editor = element; }}
          handleBeforeInput = { (chars, editorState) => {
            console.log("LLO ",chars, editorState)
          }}
          handleReturn = { (e, editorState)=> {
            console.log("FAS", {e,editorState})
            console.log("FAS", e.key, e.keyCode)
            console.log("FAS", editorState.getCurrentContent().getPlainText())
            //test.replace(/\n/g, ' ')
          }}
        />
        <MentionSuggestions
          onSearchChange={this.onSearchChange}
          suggestions={this.state.suggestions}
          entryComponent={Entry}
          displayTokens={this.state.displayTokens}
        />
      </div>
    );
  }
}