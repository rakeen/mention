import React from 'react';
// import './App.css';
import CrEditor from './CrEditor';

function App() {
  return (
    <div className="App" style={{
      minWidth: "300px",
      maxWidth: "80%",
      margin: "0 auto"
    }}>
      <CrEditor />
    </div>
  );
}

export default App;
